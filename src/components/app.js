import React, {Component} from 'react';
import Board from './board';
import Result from './result';
import InputNamePlayer from './inputNamePlayer';
import styled from 'styled-components';
import {connect} from 'react-redux';
import './styles.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.isPlay = false;
  }

  componentWillReceiveProps(nextStates, nextProps) {
    if (nextStates.state && nextStates.state.player1) {
      this.isPlay = true;
    }
    if (nextStates.state && !nextStates.state.player1) {
      this.isPlay = false;
    }
  }

  render() {
    return (
      <Div>
        {!this.isPlay &&
          <InputNamePlayer />
        }
        {this.isPlay &&
          <Div>
          <Result />
          <Board />
          </Div>
        }
      </Div>
      );
  }
}

const Div = styled.div`
  font-family: Courier New, Courier, monospace;
  margin: 0 auto;
  width: 200px;
`;

export default connect((state) => ({ state }))(App);

export {App as PureApp};

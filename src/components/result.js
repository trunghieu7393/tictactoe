import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

class Result extends Component {
  render () {
    let result = '';
    let player = this.props.player1;
    if (this.props.turn === "x") {
      player = this.props.player2;
    }
    if (this.props.turn) {
      result = `It's ${player.toUpperCase()}'s turn.`;
    }
    if (this.props.won) {
      result = `${player.toUpperCase()} win!!`
    } else if (this.props.draw) {
      result = 'draw';
    }
    return (
      <div>
        <p>
          {result}
        </p>
      </div>
    );
  }
}

Result.propTypes = {
  won: PropTypes.string,
  turn: PropTypes.string.isRequired,
  draw: PropTypes.bool.isRequired
};

export default connect(
  ({won, turn, draw, player1, player2}) => ({
    won, turn, draw, player1, player2
  })
)(Result);

export {Result as PureResult};

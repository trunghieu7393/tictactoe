import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { playGame } from '../actions/startGameAction';
import {connect} from 'react-redux';

class InputNamePlayer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      namePlayer1: "Player 1",
      namePlayer2: "Player 2"
    }
  }

  updateInputValue = () => {
    this.props.playGame(this.refs.p1.value, this.refs.p2.value);
  }

  render() {
    return (
      <div>
        <p>
          Input Your Name
        </p>
        <input ref="p1" placeholder={this.state.namePlayer1} name="p1" required/>
        <input ref="p2" placeholder={this.state.namePlayer2} name="p2" required/>
        <div>
          <button onClick={this.updateInputValue}>Play</button>
        </div>
      </div>
    );
  }
}

InputNamePlayer.propTypes = {
  namePlayer1: PropTypes.string,
  namePlayer2: PropTypes.string,
  playGame:    PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    state: state
  }
}

export default connect(mapStateToProps,
  (dispatch) => {
    return {
      playGame(namePlayer1, namePlayer2) {
        dispatch(playGame(namePlayer1, namePlayer2));
      }
    };
  }
)(InputNamePlayer);

export {InputNamePlayer as PureInputNamePlayer};

import { resultForSymbol } from '../games/logic';
import * as ACTION from "../actions/actionTypes";
import * as _ from 'lodash';

const X = 'x';
const O = 'o';

export const initialState = {
  board: {
    0: ['', '', ''],
    1: ['', '', ''],
    2: ['', '', '']
  },
  won: undefined,
  wonLine: undefined,
  draw: false,
  turn: O
};

export const gameReducer = (state, action) => {
  switch (action.type) {
    case ACTION.ADD_SYMBOL:
      const {symbol, row, position} = action;
      const newState = _.cloneDeep(state);
      newState.board[row][position] = symbol;

      const xResult = resultForSymbol(X, newState.board);
      const oResult = resultForSymbol(O, newState.board);

      if (xResult.won) {
        newState.won = X;
        newState.wonLine = xResult.line;
      }

      if (oResult.won) {
        newState.won = O;
        newState.wonLine = oResult.line;
      }

      if (!newState.won) {
        newState.turn = newState.turn === O ? X : O;
      }

      const boardIsFull = [
        ...newState.board[0],
        ...newState.board[1],
        ...newState.board[2]
      ]
        .filter(symbol => symbol !== '')
        .length === 9;

      if (boardIsFull && !newState.won) {
        newState.draw = true;
      }

      return newState;
    case ACTION.START_AGAIN:
      return initialState;
    case ACTION.PLAY:
      let {namePlayer1, namePlayer2} = action;
      const newStatePlayer = _.cloneDeep(state);
      newStatePlayer.player1 = namePlayer1;
      newStatePlayer.player2 = namePlayer2;
      return newStatePlayer;
    default:
      return state;
  }
};

import {ADD_SYMBOL, START_AGAIN, PLAY} from "./actionTypes";

export const addSymbol = (row, position, symbol) => ({
  type: ADD_SYMBOL,
  symbol,
  row,
  position
});

export const playGame = (namePlayer1, namePlayer2) => ({
  type: PLAY,
  namePlayer1,
  namePlayer2
});

export const startAgain = () => ({
  type: START_AGAIN
});
